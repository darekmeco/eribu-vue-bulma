import Vue from 'vue'
import App from './App.vue'
import Router from 'vue-router'
import Dashboard from './views/Dashboard.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
      path: '/',
      name: 'home',
      component: Dashboard
    },
    {
      path: '/about',
      name: 'about',
      component: () => import( /* webpackChunkName: "about" */ './views/About.vue')
    },

    {
      path: '/customers',
      component: App,
      children: [{
        path: 'index',
        name: 'customers',
        component: () => import( /* webpackChunkName: "about" */ './views/Customers.vue')
      }, {
        path: 'create',
        name: 'customers.create',
        component: () => import( /* webpackChunkName: "about" */ './views/CustomersForm.vue')
      }]
    },
    {
      path: '/products',
      component: App,
      children: [{
          path: 'index',
          name: 'products.index',
          component: () => import( /* webpackChunkName: "about" */ '@/modules/products/views/Index.vue')
        }, {
          path: 'create',
          name: 'products.create',
          component: () => import( /* webpackChunkName: "about" */ '@/modules/products/views/Create.vue')
        },
        {
          path: 'edit/:id',
          name: 'products.edit',
          component: () => import( /* webpackChunkName: "about" */ '@/modules/products/views/Edit.vue')
        },
        {
          path: 'category',
          component: App,
          children: [{
              path: 'init',
              name: 'products.category.init',
              component: () => import( /* webpackChunkName: "about" */ '@/modules/products/views/Index.vue')
            },
            {
              path: 'index',
              name: 'products.category.index',
              component: () => import( /* webpackChunkName: "about" */ '@/modules/products/views/CategoryIndex.vue')
            }
          ]
        },
      ]
    },

    {
      path: '/media',
      component: App,
      children: [{
        path: 'index',
        name: 'media',
        component: () => import( /* webpackChunkName: "about" */ '@/modules/media/views/Files.vue')
      }, {
        path: 'create',
        name: 'media.file.create',
        component: () => import( /* webpackChunkName: "about" */ '@/views/FilesForm.vue')
      }]
    },

  ]
})
