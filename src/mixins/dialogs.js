const Dialogs = {
  methods: {
    confirmCustomDelete(cb) {
      this.$dialog.confirm({
        title: 'Usuwanie elemetu',
        message: 'Napewno usunąć element',
        confirmText: 'Tak, usuń',
        type: 'is-danger',
        hasIcon: true,
        onConfirm: cb
      })
    },
    confirmCustomMove(cb) {
      this.$dialog.confirm({
        title: 'Przenoszenie elementu',
        message: 'Napewno przenieść element',
        confirmText: 'Tak, przenieś',
        type: 'is-danger',
        hasIcon: true,
        onConfirm: cb
      })
    },
  }
}
export default Dialogs;
