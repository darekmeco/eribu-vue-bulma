import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';

export default {
  getPathThumb(row) {
    console.log('getPathThumb', row);
    const searchpath = _get(row, 'searchpath', null);
    const path = searchpath ? `/${searchpath}/` : '/';
    return `${process.env.VUE_APP_APIURL}/images${path}`;
  },
  getPathBig(row) {
    const searchpath = _get(row, 'searchpath', null);
    const path = searchpath ? `/${searchpath}/` : '/';
    return `${process.env.VUE_APP_APIURL}/assets${path}`;
  },
  getFilePathThumb(row = {}) {
    console.log('roww: ', row);
    const filename = _get(row, 'filename', null);
    let searchpath = _get(row, 'searchpath', null);
    searchpath = _isEmpty(searchpath) ? '/' : '/' + searchpath;
    console.log('searchpath:', searchpath);
    const path = searchpath + '/' + filename;
    console.log('pathh: ', path);
    return `${process.env.VUE_APP_APIURL}/images${path}`;
  },
  getFilePathBig(row) {
    const filename = _get(row, 'filename', null);
    let searchpath = _get(row, 'searchpath', null);
    searchpath = _isEmpty(searchpath) ? '/' : '/' + searchpath;
    const path = searchpath + '/' + filename;
    return `${process.env.VUE_APP_APIURL}/assets${path}`;
  },
  getPathFromObject(o, path, defpath = '/') {
    return _get(o, path, defpath);
  }
}
