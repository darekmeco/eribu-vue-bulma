import FileMultiChooser from '@/modules/media/components/FileMultiChooser';
import FileChooser from '@/modules/media/components/FileChooser';
import _isEqual from 'lodash/isEqual';
import {
  TreeSelect
} from 'ant-design-vue';
import 'ant-design-vue/lib/tree-select/style';

const Products = {
  data() {
    return {
      treeData: [],
      categories: [],
      form: {
        _id: null,
        name: '',
        description: '',
        thumb: {
          filename: '',
          isLoaded: '',
          path: '',
          searchpath: ''
        },
        categories: [],
        gallery: [],
      }
    }
  },
  methods: {
    async fetchData() {
      const res = await this.$axios.get(`${process.env.VUE_APP_APIURL}/product/category/tree`);
      this.treeData = res.data;
      console.log('fetch: ', this.treeData);
    },
    onChange(value) {
      console.log('onChange ', value)
      console.log('onChange form ', this.form)
      this.form.categories = value
    },
    validateBeforeSubmit() {
      console.log('form', this.form);
      this.$validator.validateAll().then((result) => {
        console.log(result);
        if (result) {

          this.$toast.open({
            message: 'Form is valid!',
            type: 'is-success',
            position: 'is-bottom'
          })

          this.submitForm();
          return;
        }

        this.$toast.open({
          message: 'Formularz zawiera błędy.',
          type: 'is-danger',
          position: 'is-bottom'
        })
      });
    },
    /**
     * Set default Choose files attributes to store
     */
    updateStore() {
      console.log('updateStore', this.form.gallery);
      /**
       * Commit to State makes Array this.form.gallery responsive (strange|check)
       */
      this.$store.commit('filesArray', this.form.gallery);
      /**
       * Commit to State NOT makes Object responsive (strange|check)
       */
      this.$store.commit('fileObject', this.form.thumb);
    }

  },
  computed: {
    checkedStrategy() {
      return TreeSelect.SHOW_ALL;
    },
    choosedFile() {
      console.log('choosedFile()', this.$store.state.choosedFile);
      return this.$store.state.choosedFile;
    },
    choosedFiles() {
      console.log('choosedFiles()', this.$store.state.choosedFiles);
      return this.$store.state.choosedFiles;
    }
  },
  watch: {
    /**
     * Update Form thumb if different.
     * @param  {Object} newval [description]
     */
    choosedFile(newval) {
      console.log('watch choosedFile', newval, this.form.thumb, _isEqual(newval, this.form.thumb));
      /**
       * This code is used bacause of strange vuex behavior (responsive arrays by not objects)
       */
      if (_isEqual(newval, this.form.thumb) === false) {
        console.log('watch choosedFile not equal');
        this.$set(this.form, 'thumb', newval);
      }
    },
    /**
     *
     * Watch. but not update bacause of strange vuex behavior (responsive arrays by not objects)
     * can be usefull: this.$set(this.form, 'gallery', this.choosedFiles);
     * @param  {Array} newval [description]
     */
    choosedFiles(newval) {
      console.log('watch choosedFiles', newval, this.form.gallery, _isEqual(newval, this.form.gallery));
    }
  },
  async mounted() {
    this.updateStore();
    this.fetchData();
  },
  components: {
    FileMultiChooser,
    FileChooser,
    'a-tree-select': TreeSelect,
  }
}
export default Products;
