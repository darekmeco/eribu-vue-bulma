import Vue from 'vue'
import Vuex from 'vuex'
import _isArray from 'lodash/isArray';
import _findIndex from 'lodash/findIndex';

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  state: {
    choosedFile: {
      filename: '',
      isLoaded: '',
      path: '',
      searchpath: ''
    },
    choosedFiles: []
  },
  mutations: {
    fileObject(state, val) {
      //Object.assign(state.choosedFile, val);
      state.choosedFile = Object.assign({}, state.choosedFile, val)
      //state.choosedFile = val;
      console.log('fileObject state:', state);
    },
    changeFileObject(state, val) {
      console.log('changeFileObject state:', state, val);
      state.choosedFile = Object.assign({}, state.choosedFile, val);

      //state.choosedFile = val;
    },
    filesObject(state, val) {
      const choosed = state.choosedFiles;
      choosed.push(val);
      state.choosedFiles = choosed;
      console.log('state:', state);
    },
    filesArray(state, val) {
      state.choosedFiles = val;
      console.log('filesArray state:', state);
    },
    removeFromFilesArray(state, object) {
      console.log('removeFromFilesArray commit', state.choosedFiles);
      if (_isArray(state.choosedFiles) === true) {
        const removedIdx = _findIndex(state.choosedFiles, (n) => {
          return object.filename == n.filename
        });
        console.log('idx', removedIdx);
        //console.log('this.fileObjects', this.fileObjects);
        Vue.delete(state.choosedFiles, removedIdx);
      }
    }
  },
  getters: {
    // getFileObject: state => {
    //   return state.choosedFile;
    // }
  },
  actions: {

  }
})
